<?php
include_once 'dao.php';
include_once 'authenticate.php';

if (!valid()) {
    header('Location: ' . $_SERVER['HTTP_REFERER']);
    //save form data
}
global $dao;
$project = new ProjectData($_POST['pid'], null, null, []);
$dao->deleteProject($project);
header('Location: /cms.php');

function valid()
{
    if (!isset($_POST)) {
        return false;
    } else if (!isset($_POST['pid'])) {
        return false;
    }
    return true;
}
