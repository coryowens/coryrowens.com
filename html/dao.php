<?php
include 'projectdata.php';
class Dao
{
    protected $db;
    public function __construct()
    {
        $host     = $_SERVER['CORYROWENSDBHOST'];
        $username = $_SERVER['CORYROWENSDBUSERNAME'];
        $password = $_SERVER['CORYROWENSDBPASSWORD'];
        $database = $_SERVER['CORYROWENSDB'];
        $this->db = new PDO('mysql:host=' . $host . ';dbname=' . $database . ';charset=utf8mb4', $username, $password);
    }

    public function getProjects()
    {
        $sql  = "CALL GetProjects();";

        $rows = $this->db->query($sql)->fetchAll(PDO::FETCH_ASSOC);

        if (!$rows) {
            return [];
        }
        $projects = [];
        foreach ($rows as $row) {
            array_push($projects, new Projectdata($row['projectid'], $row['projectname'], $row['shortdescription'], null));
        }
        foreach ($projects as $project) {
            $links          = $this->getLinks($project->projectid);
            $project->links = $links;

        }
        return $projects;
    }

    public function getProject($pid)
    {
        $sth = $this->db->prepare("select * from projects where projectid = :pid;");
        $sth->bindParam(':pid', $pid);
        $sth->execute();
        $rows = $sth->fetchAll(PDO::FETCH_ASSOC);
        if (!$rows) {
            return null;
        }
        $projects = [];
        foreach ($rows as $row) {
            array_push($projects, new Projectdata($row['projectid'], $row['projectname'], $row['shortdescription'], null));
        }
        foreach ($projects as $project) {
            $links          = $this->getLinks($project->projectid);
            $project->links = $links;
        }
        return $projects[0];
    }

    public function getLinks($pid)
    {

        $sql  = "CALL GetLinks(" . $pid . ");";
        $rows = $this->db->query($sql)->fetchAll(PDO::FETCH_ASSOC);
        if (!$rows) {
            return [];
        }
        $links = [];
        foreach ($rows as $row) {
            array_push($links, $row['url']);
        }
        return $links;
    }

    public function auth($user, $pw)
    {
        $sth = $this->db->prepare("call Auth(:user, :pw);");
        $sth->bindParam(':user', $user);
        $sth->bindParam(':pw', $pw);
        $sth->execute();
        $rows = $sth->fetchAll(PDO::FETCH_ASSOC);
        if (!$rows) {
            return false;
        } else {
            return true;
        }
    }

    public function getPID($project){
        $sql  = "SELECT projectid FROM projects WHERE projectname=:pn AND shortdescription=:sd;";
        $sth = $this->db->prepare($sql);
        $sth->bindParam(':pn', $project->projectname);
        $sth->bindParam(':sd', $project->shortdescription);
        $sth->execute();
        $rows = $sth->fetchAll(PDO::FETCH_ASSOC);
        if (!$rows) {
            return -1;
        } else {
            return $rows[0]['projectid'];
        }
    }

    public function addProject($project){
        if(!(isset($project) && isset($project->projectname) && isset($project->shortdescription))){
            return;
        }
        $sql  = "CALL AddProject(:pn, :sd);";
        $sth = $this->db->prepare($sql);
        $sth->bindParam(':pn', $project->projectname);
        $sth->bindParam(':sd', $project->shortdescription);
        $sth->execute();
        $pid = $this->getPID($project);
        echo $pid;
        print_r($project->links);
        foreach ($project->links as $link) {
            echo $link;
            $sql  = "CALL AddLink(:pid, :url);";
            $sth = $this->db->prepare($sql);
            $sth->bindParam(':pid', $pid);
            $sth->bindParam(':url', $link);
            $sth->execute();
        }
    }

    public function updateProject($project){
        $this->deleteProject($project);
        $this->addProject($project);
    }

    public function deleteProject($project){
        $sql  = "CALL DeleteProject(:pid);";
        $sth = $this->db->prepare($sql);
        $sth->bindParam(':pid', $project->projectid);
        $sth->execute();
    }

}

$dao = new Dao();
