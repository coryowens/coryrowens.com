<?php
include 'html.php';
include 'header.php';
include 'content.php';
include 'footer.php';
include_once 'dao.php';
include_once 'authenticate.php';
startHTML('pedit.js');
//BEGIN BODY
printHeader(1);
startContent();
//BEGIN CONTENT
printProject();
//END CONTENT
endContent();
printFooter();
//END BODY
endHTML();

function printProject()
{
    global $dao;
    if (!isset($_GET['pid'])) {
        header('Location: /');
    }
    $pid = $_GET['pid'];
    if ($pid != 'new') {
        $project = $dao->getProject($pid);
        if (!$project) {
            header('Location: /');
        }
    } else {
        $project = new ProjectData('new', '', '', []);
    }

    echo '
            <div class="breadcrumb">
                <p>
                    <a href="cms.php">
                        CMS
                    </a>
                    &gt;
                    <a href="project.php?pid=' . $project->projectid . '">
                        ' . $project->projectname . '
                    </a>
                </p>
            </div>
            <div class="edit">
                <form method=post action="projectsave.php">
                <table>
                <tr><td>Project ID:</td><td><input type="text" name="pid" value="' . $project->projectid . '" readonly></td></tr>
                <tr><td>Project Name:</td><td><input type="text" name="name" value="' . $project->projectname . '"></td></tr>
                <tr><td>Project Short Description:</td><td><textarea class="multiline" name="sd" cols="30" rows="5">' . $project->shortdescription . '</textarea></td></tr>
        ';
    if (count($project->links) == 0) {
        echo '<tr id="linkrow0"><td>Link:<td><input type="text" name="links[0]" value="" size="40"><input type="button" onclick="removeLink(0)" value="X"><td></tr>';
    } else {
        for ($i = 0; $i < count($project->links); $i++) {
            $link = $project->links[$i];
            echo '<tr id="linkrow' . $i . '"><td>Link:<td><input type="text" name="links[' . $i . ']" value="' . $link . '" size="40"><input type="button" onclick="removeLink(' . $i . ')" value="X"><td></tr>';
            echo '<script>next=' . $i . '</script>';
        }
    }
    echo '

                </table>
                <input type="button" value="Add Link" class="addlink" onclick="addLink();">
                <input type="submit" value="Save">
                </form>
            </div>
        ';
}


