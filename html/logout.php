<?php

session_start();
$_SESSION['username']   = null;
$_SESSION['authorized'] = false;
session_unset();
session_destroy();
header('Location: cmslogin.php');
