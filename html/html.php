<?php
function startHTML($script=null)
{
    echo '
			<html>
		    <head profile="http://coryrowens.com">
		        <link rel="stylesheet" type="text/css" href="reset.css"/>
		        <link rel="stylesheet" type="text/css" href="style.css"/>
		        <link rel="icon" type="image/x-icon" href="img/favicon.ico"/>
      			<link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico"/>
      			<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
      ';
    if (isset($script)) {
        echo '<script type="text/javascript" src="' . $script . '"></script>';
    }
    echo '
	    <title>
	    CoryROwens.com
	    </title>
	    </head>
	    <body>
	    <divid = "container">
    ';
}

function endHTML()
{
    echo '
    </div>
    </body>
    </html>
    ';
}
