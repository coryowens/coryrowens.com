<?php
include 'html.php';
include 'header.php';
include 'content.php';
include 'footer.php';
// include 'authenticate.php';
require 'authenticate.php';
startHTML('cmsscripts.js');
//BEGIN BODY
printHeader(0);
startContent();
//BEGIN CONTENT
printCMS();
//END CONTENT
endContent();
printFooter();
//END BODY
endHTML();

function printCMS()
{
    printLogout();
    printPortfolioCRUD();
}

function printPortfolioCRUD()
{
    global $dao;
    $projects = $dao->getProjects();
    echo '
        <div class="portfolioCRUD">
            <table>
            <th><td>Project</td></th>
    ';

    foreach ($projects as $project) {
        echo '<tr><td><a href="/projectedit.php?pid=' . $project->projectid . '">' . $project->projectname . '</a><input type="button" onclick="removeProject(' . $project->projectid . ')" value="X"></td></tr>';
    }

    echo '
            </table>
            <form method="get" action="projectEdit.php">
                <input type="hidden" name="pid" value="new">
                <input type="submit" id="createProjectButton" value="Create Project" >
            </form>
        </div>
    ';
}
