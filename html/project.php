<?php
include 'html.php';
include 'header.php';
include 'content.php';
include 'footer.php';
include 'dao.php';
startHTML();
//BEGIN BODY
printHeader(1);
startContent();
//BEGIN CONTENT
printProject();
//END CONTENT
endContent();
printFooter();
//END BODY
endHTML();

function printProject()
{
    global $dao;
    if (!isset($_GET['pid'])) {
        header('Location: /');
    }
    $pid     = $_GET['pid'];
    $project = $dao->getProject($pid);
    if (!$project) {
        header('Location: /');
    }
    echo '
            <div class="breadcrumb">
                <p>
                    <a href="portfolio.php">
                        Portfolio
                    </a>
                    &gt;
                    <a href="project.php?pid=' . $project->projectid . '">
                        ' . $project->projectname . '
                    </a>
                </p>
            </div>
            <div class="fullpreview">
                <div class="previewimage">
                        <img src="img/placeholder.jpg"/>
                </div>
                <div class="previewdescription">
                    <h2>
                        <a href="project.php">
                            ' . $project->projectname . '
                        </a>
                    </h2>
                    <p>
                        ' . $project->shortdescription . '
                    </p>
                </div>
                <div class="links">
                    <h2>
                        Links
                    </h2>
                    <p><ul>
        ';
    foreach ($project->links as $link) {
        echo '<li><a href="' . $link . '">' . $link . '</a></li>';
    }
    echo '
                    </ul></p>
                </div>
            </div>
        ';

}
