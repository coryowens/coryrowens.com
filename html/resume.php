<?php
include 'html.php';
include 'header.php';
include 'content.php';
include 'footer.php';
startHTML();
//BEGIN BODY
printHeader(2);
startContent();
//BEGIN CONTENT
printResume();
//END CONTENT
endContent();
printFooter();
//END BODY
endHTML();

function printResume()
{
    echo '
			<div id="content">
                <div class="downloadresume">
                    <a href="doc/Resume.pdf">
                        Download as PDF
                    </a>
                </div>
                <div class="webresume">
                    <object data="doc/Resume.pdf" type="application/pdf" width="100%" height="100%">
                        <p>
                            It appears you don\'t have a PDF plugin for this browser. Download the PDF of the resume above.
                        </p>
                    </object>
                </div>
            </div>
		';
}
