<?php
class ProjectData
{
    public $projectid;
    public $projectname;
    public $shortdescription;
    public $links;
    public function __construct($pid, $pn, $sd, $l)
    {
        $this->projectid        = $pid;
        $this->projectname      = $pn;
        $this->shortdescription = $sd;
        $this->links            = $l;
    }

}
