function removeProject(pid) {
    if (confirm('Are you sure you want to delete this project?')) {
        var form = document.createElement("form");
        form.setAttribute("method", 'post');
        form.setAttribute("action", 'projectdelete.php');
        var hiddenField = document.createElement("input");
        hiddenField.setAttribute("type", "hidden");
        hiddenField.setAttribute("name", 'pid');
        hiddenField.setAttribute("value", pid);
        form.appendChild(hiddenField);
        document.body.appendChild(form);
        form.submit();
    } else {
        // Do nothing!
    }
}