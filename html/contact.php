<?php
include 'html.php';
include 'header.php';
include 'content.php';
include 'footer.php';
startHTML();
//BEGIN BODY
printHeader(3);
startContent();
//BEGIN CONTENT
printContact();
//END CONTENT
endContent();
printFooter();
//END BODY
endHTML();

function printContact()
{
    echo '
		<div id="content">
	        <div class="contact">
	            <ul>
	                <li><a href="mailto:cory@mockupaddress.com">cory@mockupaddress.com</a></li>
	                <li><a href="http://linkedin.com">LinkedIn</a></li>
	                <li><a href="http://plus.google.com">Google+</a></li>
	                <li><a href="http://twitter.com">Twitter</a></li>
	            </ul>
	        </div>
	    </div>
	';
}
