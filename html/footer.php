<?php
function printFooter()
{
    echo '
            <!-- BEGIN FOOTER -->
            <div id="footer">
                <div class="copyright">
                    &copy; 2016 Cory Owens
                </div>
                <div class="cms">
                    <a href="cms.php">
                        CMS
                    </a>
                </div>
            </div>
            <!-- END FOOTER -->
        ';
}
