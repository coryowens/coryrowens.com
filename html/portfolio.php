<?php
include 'html.php';
include 'header.php';
include 'content.php';
include 'footer.php';
include 'dao.php';
startHTML();
//BEGIN BODY
printHeader(1);
startContent();
//BEGIN CONTENT
printPortfolio();
//END CONTENT
endContent();
printFooter();
//END BODY
endHTML();

function printPortfolio()
{
    global $dao;
    $projects = $dao->getProjects();
    foreach ($projects as $project) {
        echo '
            <div class="gallerypreview">
                <div class="previewimage">
                    <a href="project.php?pid=' . $project->projectid . '">
                        <img src="img/placeholder.jpg"/>
                    </a>
                </div>
                <div class="previewdescription">
                    <h2>
                        <a href="project.php' . $project->projectid . '">
                            ' . $project->projectname . '
                        </a>
                    </h2>
                    <p>
                        ' . $project->shortdescription . '
                    </p>
                </div>
            </div>
        ';
    }

}
