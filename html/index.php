<?php
include 'html.php';
include 'header.php';
include 'content.php';
include 'footer.php';
startHTML();
//BEGIN BODY
printHeader(0);
startContent();
//BEGIN CONTENT
printPortfolio();
//END CONTENT
endContent();
printFooter();
//END BODY
endHTML();

function printPortfolio()
{
    echo '
            <div class="gallerypreview">
                    <div class="previewimage">
                        <a href="project.php">
                            <img src="img/placeholder.jpg"/>
                        </a>
                    </div>
                    <div class="previewdescription">
                        <h2>
                            <a href="project.php">
                                HelloWorld
                            </a>
                        </h2>
                        <p>
                            Lorem ipsum dolor sit amet.
                        </p>
                    </div>
                </div>
                <div class="gallerypreview">
                    <div class="previewimage">
                        <a href="project.php">
                            <img src="img/placeholder.jpg"/>
                        </a>
                    </div>
                    <div class="previewdescription">
                        <h2>
                            <a href="project.php">
                                Foo.js
                            </a>
                        </h2>
                        <p>
                            Lorem ipsum dolor sit amet.
                        </p>
                    </div>
                </div>
                <div class="gallerypreview">
                    <div class="previewimage">
                        <a href="project.php">
                            <img src="img/placeholder.jpg"/>
                        </a>
                    </div>
                    <div class="previewdescription">
                        <h2>
                            <a href="project.php">
                                iBar
                            </a>
                        </h2>
                        <p>
                            Lorem ipsum dolor sit amet.
                        </p>
                    </div>
                </div>
                <div class="gallerypreview">
                    <div class="previewimage">
                        <a href="project.php">
                            <img src="img/placeholder.jpg"/>
                        </a>
                    </div>
                    <div class="previewdescription">
                        <h2>
                            <a href="project.php">
                                FizzBuzz
                            </a>
                        </h2>
                        <p>
                            Lorem ipsum dolor sit amet.
                        </p>
                    </div>
                </div>
                <div class="gallerypreview">
                    <div class="previewimage">
                        <a href="project.php">
                            <img src="img/placeholder.jpg"/>
                        </a>
                    </div>
                    <div class="previewdescription">
                        <h2>
                            <a href="project.php">
                                1337++
                            </a>
                        </h2>
                        <p>
                            Lorem ipsum dolor sit amet.
                        </p>
                    </div>
                </div>
        ';
}
