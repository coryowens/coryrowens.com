<?php
include_once 'dao.php';
include_once 'authenticate.php';

if (!valid()) {
    header('Location: ' . $_SERVER['HTTP_REFERER']);
    //save form data
}
global $dao;
$project = new ProjectData($_POST['pid'], $_POST['name'], $_POST['sd'], $_POST['links']);
if ($_POST['pid'] == 'new') {
    $dao->addProject($project);
} else {
    $dao->updateProject($project);
}
header('Location: /cms.php');

function valid()
{
    if (!isset($_POST)) {
        return false;
    } else if (!isset($_POST['name']) || !isset($_POST['sd'])) {
        return false;
    } else if ($_POST['name'] == '' || $_POST['sd'] == '') {
        return false;
    }
    return true;
}
