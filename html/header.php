<?php
function printHeader($cur)
{
    echo '
            <!-- BEGIN HEADER -->
            <div id="header">
                <h1 class="logo">
                    <a href="/">
                        CoryROwens
                    </a>
                </h1>
                <ul class="nav">
                    <li>
                        <a href="portfolio.php"' . ($cur == 1 ? 'class="current"' : '') . '>Portfolio</a>
                    </li>
                    <li>
                        <a href="resume.php"' . ($cur == 2 ? 'class="current"' : '') . '>Resume</a>
                    </li>
                    <li>
                        <a href="contact.php"' . ($cur == 3 ? 'class="current"' : '') . '>Contact</a>
                    </li>
                </ul>
            </div>
            <!-- END HEADER -->
        ';
}
