<?php
include_once 'dao.php';
if (!isset($_SESSION)) {
    session_start();
}

authenticate();
function authenticate()
{
    global $dao;
    $authorized = false;
    if (isset($_POST['username']) && isset($_POST['password'])) {
        $authorized = $dao->auth($_POST['username'], $_POST['password']);
        if ($authorized) {
            allow($_POST['username']);
            header('Location: '.$_SERVER['REQUEST_URI']);
        } else {
            deny();
        }
    } else if (isset($_SESSION['username'])
        && isset($_SESSION['authorized'])
        && $_SESSION['authorized'] == true) {
        allow($_SESSION['username']);
    } else {
        deny();
    }
}

function allow($username)
{
    $_SESSION['username']   = $username;
    $_SESSION['authorized'] = true;
}

function deny(){
    unset($_SESSION['username']);
    unset($_SESSION['authorized']);
    header('Location: cmslogin.php');
}

function printLogout()
{
    echo '
        <div class="logout">
            <p><a href="logout.php">Logout</a></p>
        </div>
    ';
}