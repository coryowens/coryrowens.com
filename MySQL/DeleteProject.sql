USE coryrowens;
DELIMITER $$
DROP PROCEDURE IF EXISTS coryrowens.DeleteProject$$
CREATE PROCEDURE DeleteProject(pid INT UNSIGNED)
	BEGIN
		DELETE FROM projects WHERE projectid = pid;
	END$$
DELIMITER ;
