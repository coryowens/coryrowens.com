USE coryrowens;
DELIMITER $$
DROP PROCEDURE IF EXISTS coryrowens.GetProjects$$
CREATE PROCEDURE GetProjects()
	BEGIN
		SELECT * FROM projects;
	END$$
DELIMITER ;
