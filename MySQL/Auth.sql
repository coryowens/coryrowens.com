USE coryrowens;
DELIMITER $$
DROP PROCEDURE IF EXISTS coryrowens.Auth$$
CREATE PROCEDURE Auth(user varchar(32), pw varchar(512))
	BEGIN
		SELECT userid FROM users WHERE username = user AND password = password(pw);
	END$$
DELIMITER ;
