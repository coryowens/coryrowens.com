USE coryrowens;
DELIMITER $$
DROP PROCEDURE IF EXISTS coryrowens.AddProject$$
CREATE PROCEDURE AddProject(pn VARCHAR(512), sd VARCHAR(2048))
	BEGIN
		INSERT INTO projects (projectname, shortdescription) VALUES(pn, sd);
	END$$
DELIMITER ;
