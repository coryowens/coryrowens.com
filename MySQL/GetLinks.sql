USE coryrowens;
DELIMITER $$
DROP PROCEDURE IF EXISTS coryrowens.GetLinks$$
CREATE PROCEDURE GetLinks(pid INT UNSIGNED)
	BEGIN
		SELECT * FROM projectlinks where projectid = pid;
	END$$
DELIMITER ;
