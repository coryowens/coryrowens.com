USE coryrowens;
DELIMITER $$
DROP PROCEDURE IF EXISTS coryrowens.DeleteLink$$
CREATE PROCEDURE DeleteLink(pid INT UNSIGNED, u VARCHAR(512))
	BEGIN
		DELETE FROM projectlinks WHERE projectid = pid AND url = u;
	END$$
DELIMITER ;
