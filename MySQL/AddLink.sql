USE coryrowens;
DELIMITER $$
DROP PROCEDURE IF EXISTS coryrowens.AddLink$$
CREATE PROCEDURE AddLink(pid INT UNSIGNED, url VARCHAR(512))
	BEGIN
		INSERT INTO projectlinks (projectid, url) VALUES(pid, url);
	END$$
DELIMITER ;
