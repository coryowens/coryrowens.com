create database if not exists coryrowens;
use coryrowens;
create table if not exists users (
	userid int unsigned not null auto_increment primary key
	, username varchar(32) not null 
	, password varchar(512) not null
);
create table if not exists projects (
	projectid int unsigned not null auto_increment primary key
	, projectname varchar(512) not null 
	, shortdescription varchar(2048) not null
);
create table if not exists projectlinks (
	projectid int unsigned not null, foreign key (projectid) references projects(projectid) on delete cascade
	, url varchar(512) not null 
);
